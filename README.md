# Large Paintings
Large Paintings adds new blocks that let you display paintings in-game. Available on CurseForge: https://www.curseforge.com/minecraft/mc-mods/large-paintings
## How to use
1. Craft the painting block, paintbrush, and canvas blocks (31).
2. Assemble the paintings in a 4-tall, 8-wide multiblock, with the painting block at the lower-left.
3. Right-click the painting block with the paintbrush.
## Current limitations
- You can only build one size of painting: a 4x8 landscape. But other sizes are coming!
- You can only have one image. This can be customized with a resource pack. (Download the template here!)
## Assembling the structure
1. Place the 31 canvas blocks 4-tall by 8-wide, leaving the bottom-left space open for the painting block  
2. Place the painting block in the bottom-left space
3. Right-click with the paintbrush. Right-click again to disable the painting.
   ![alt text](web/recipe_structure.png "4x8 canvas blocks with a painting block in the bottom left")

## Recipes
Canvas block\
![alt text](web/recipe_canvas.png "8 wool surrounding a wooden pressure plate yields 8 canvas blocks")

Painting block\
![alt text](web/recipe_painting.png "8 sticks surrounding a canvas block yields a painting block")

Paintbrush\
![alt text](web/recipe_paintbrush.png "3 string in top row, wooden plank centered in middle row, and a wooden stick centered in the bottom row yields a paintbrush")

## I don't like your image and want to use my own
- The image is easily overwritten with a resource pack. You can download the template [here](https://www.curseforge.com/minecraft/texture-packs/large-paintings-template).
- For the current structure size (4x8), the image needs to be:
   - Square, divisible by 16. Examples: 512x512, 1024x1024, 2048x2048, etc.
   - Transparent on the top half. The actual image should be the bottom half of the image file. See below.\
  ![alt text](web/example_image_file.png "3 string in top row, wooden plank centered in middle row, and a wooden stick centered in the bottom row yields a paintbrush")

## Future roadmap
- Allow multiple images to be used with the ability to choose or cycle through them
- Allow multiple sizes to be used instead of just 4x8
- Data Generator for creating the JSON files
- Add configs for
   - Breaking a part of a displayed painting disables all other displayed canvas blocks
   - Breaking a part of a displayed painting drops all other displayed canvas blocks
 
## Issues?
- [Submit an issue / question / suggestion here](https://gitlab.com/chirptheboy/large-paintings/-/issues/new).

## Images
"A mural on the Road to Hana Maui." by Bernard Spragg is marked under CC0 1.0.\
"The Moon tonight" by JanetR3 is licensed with CC BY 2.0.\
"mountain" by barnyz is licensed with CC BY-NC-ND 2.0.\
"Ocean Morning" by ralph and jenny is licensed with CC BY 2.0.\
"Mangifera indica (Common mango)" by swtexture.com is licensed with CC BY 4.0\
"Minecraft Pixel Map" by Andrew Mason is licensed with CC BY 2.0\
"Underwater life" by Michal, Wojtek, and Jerzy Strzelecki licensed with CC BY 3.0\  
"gloomy forest" by gorchakov.artem is licensed with CC BY 2.0\
"Painted Desert" by Paul Fundenburg is licensed with CC BY 2.0 

To view a copy of these licenses, visit
   
   - https://creativecommons.org/licenses/cc0/1.0/
   - https://creativecommons.org/licenses/by/2.0/
   - https://creativecommons.org/licenses/by/3.0/
   - https://creativecommons.org/licenses/by/4.0/
