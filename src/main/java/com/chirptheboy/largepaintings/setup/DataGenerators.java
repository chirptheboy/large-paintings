package com.chirptheboy.largepaintings.setup;

import com.chirptheboy.largepaintings.data.BlockStates;
import com.chirptheboy.largepaintings.data.Recipes;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGenerators {

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        if (event.includeServer()) {
            generator.addProvider(true, (DataProvider) new Recipes(generator));
            //generator.addProvider(new LootTables(generator));
        }

        if (event.includeClient()) {
            generator.addProvider(true, new BlockStates(generator, event.getExistingFileHelper()));
        //  generator.addProvider(new Items(generator, event.getExistingFileHelper()));
        }

    }
}