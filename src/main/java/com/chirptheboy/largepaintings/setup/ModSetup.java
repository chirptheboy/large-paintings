package com.chirptheboy.largepaintings.setup;

import com.chirptheboy.largepaintings.LargePaintings;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = LargePaintings.modID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ModSetup {

    public static final CreativeModeTab ITEM_GROUP = new CreativeModeTab("largepaintings") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(Registration.PAINTING_BLOCK.get());
        }
    };

}
