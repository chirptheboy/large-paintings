package com.chirptheboy.largepaintings.setup;

import com.chirptheboy.largepaintings.block.CanvasBlock;
import com.chirptheboy.largepaintings.block.PaintingControllerBlock;
import com.chirptheboy.largepaintings.item.PaintBrushItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import static com.chirptheboy.largepaintings.LargePaintings.modID;

public class Registration {

    private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, modID);
    private static final DeferredRegister<Item>  ITEMS  = DeferredRegister.create(ForgeRegistries.ITEMS,  modID);

    public static void init() {
        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<PaintingControllerBlock> PAINTING_BLOCK = BLOCKS.register("painting_block", PaintingControllerBlock::new);
    public static final RegistryObject<CanvasBlock>             CANVAS_BLOCK   = BLOCKS.register("canvas_block",   CanvasBlock::new);

    public static final RegistryObject<Item> PAINTING_BLOCK_ITEM = ITEMS.register("painting_block",
            () -> new BlockItem(PAINTING_BLOCK.get(), new Item.Properties().tab(ModSetup.ITEM_GROUP)));
    public static final RegistryObject<Item> CANVAS_BLOCK_ITEM = ITEMS.register("canvas_block",
            () -> new BlockItem(CANVAS_BLOCK.get(), new Item.Properties().tab(ModSetup.ITEM_GROUP)));


    public static final RegistryObject<PaintBrushItem> PAINT_BRUSH = ITEMS.register("paint_brush", PaintBrushItem::new);

}
