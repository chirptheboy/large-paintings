package com.chirptheboy.largepaintings.setup;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber
public class Config {

    public static final String CATEGORY_GENERAL = "general";

    public static ForgeConfigSpec CLIENT_CONFIG;

    public static ForgeConfigSpec.IntValue NUMBER_OF_PAINTINGS;
    public static ForgeConfigSpec.ConfigValue<List<? extends String>> PAINTINGS_AND_SIZES;

    static {
        ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();

        CLIENT_BUILDER.comment("General settings").push(CATEGORY_GENERAL);

        NUMBER_OF_PAINTINGS = CLIENT_BUILDER
                .comment("Number of paintings in the resource pack")
                .defineInRange("number_of_paintings", 8, 1, 8);

/*        PAINTINGS_AND_SIZES = CLIENT_BUILDER
                .comment("\nComma-separated list of paintings and their sizes in the format \"Painting number : Width x Height\"\nExamples '1:2x3' is painting_1 with size 2 wide by 3 tall\n         '2:4x4' is painting_2 with size 4 wide by 4 tall")
                .defineList("paintings_and_sizes", Lists.newArrayList("1:2x4"), o -> o instanceof String);
        CLIENT_BUILDER.pop();
*/

        CLIENT_CONFIG = CLIENT_BUILDER.build();
    }

}
