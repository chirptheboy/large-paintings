package com.chirptheboy.largepaintings.item;

import com.chirptheboy.largepaintings.setup.ModSetup;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;

import javax.annotation.Nullable;


public class PaintBrushItem extends Item {

    public PaintBrushItem() {
        super(new Item.Properties()
                .stacksTo(64)
                .tab(ModSetup.ITEM_GROUP));
    }

/*
    @Override
    public ActionResultType onItemUseFirst(ItemStack stack, ItemUseContext context) {

        World world = context.getPlayer().getCommandSenderWorld();

        if (!world.isClientSide) {

            PlayerEntity player = context.getPlayer();
            BlockPos blockLookingAtPos = context.getClickedPos();//(player, false);
            Block blockLookingAt       = BlockLookingAt(player, blockLookingAtPos);

            if (blockLookingAt instanceof PaintingBlock) {
                if (blockLookingAt.getBlock().getRegistryName().toString().equalsIgnoreCase("largepaintings:painting_block")) {

                    LOGGER.info("Looking at position " + blockLookingAtPos);
                    LOGGER.info("Looking at block " + blockLookingAt.getBlock());
                    LOGGER.info("With blockstate " + blockLookingAt.getBlock().getStateDefinition().any());

                    BlockState state = blockLookingAt.getStateDefinition().any();
                    Boolean enabled = state.getValue(ENABLED);
                    LOGGER.info(" Enabled: " + enabled);
                    LOGGER.info("Inverted: " + !enabled);
                    state.cycle(ENABLED);//.setValue(ENABLED, !enabled);

                    //BlockState blockState = new BlockState(blockLookingAt, blockLookingAt.getStateDefinition().any().setValue(ENABLED, !enabled));

                    world.setBlock(blockLookingAtPos, state, 10);


                    LOGGER.info("Enabled: " + enabled);
                    Boolean toggle = !enabled;
                    LOGGER.info("Change-to-state: " + toggle);
                    state.setValue(ENABLED, toggle);
                    LOGGER.info("Changed-but-not-set: " + state.getValue(ENABLED));
                    context.getPlayer().getCommandSenderWorld().setBlockAndUpdate(blockLookingAtPos,
                            blockLookingAt.getStateDefinition().any().setValue(ENABLED, !blockLookingAt.getStateDefinition().any().getValue(ENABLED)));

                    context.getPlayer().getCommandSenderWorld().getBlockState(blockLookingAtPos);
                    LOGGER.info("Changed-state: " + context.getPlayer().getCommandSenderWorld().getBlockState(blockLookingAtPos));

                }
            }
        }
        return super.onItemUseFirst(stack, context);
    }
*/


    @Nullable
    public Block BlockLookingAt(Player player, BlockPos blockPos){
        BlockState blockstate = player.level.getBlockState(blockPos);
        return blockstate.getBlock();
    }

    @Nullable
    public BlockPos BlockLookingAtPos(Player player, boolean isFluid) {
        HitResult block =  player.pick(20.0D, 0.0F, isFluid);

        if(block.getType() == HitResult.Type.BLOCK) {
            BlockPos blockpos = ((BlockHitResult)block).getBlockPos();
            return blockpos;
        }
        else
            return null;
    }
}
