package com.chirptheboy.largepaintings.block;

import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;

public abstract class PaintingBlock extends Block {

    public static final int positions = 64;
    public static final int paintings = 8;
    public static final int max_rows = 8;
    public static final int max_cols = 8;

    public static final DirectionProperty FACING = BlockStateProperties.FACING;
    public static final IntegerProperty PAINTING_ID = IntegerProperty.create("painting_id", 0, paintings);
    public static final IntegerProperty POSITION = IntegerProperty.create("position", 0, positions - 1);

    protected static final VoxelShape EAST_OPEN_AABB = Block.box(0.0D, 0.0D, 0.0D, 1.0D, 16.0D, 16.0D);
    protected static final VoxelShape WEST_OPEN_AABB = Block.box(15.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D);
    protected static final VoxelShape SOUTH_OPEN_AABB = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 1.0D);
    protected static final VoxelShape NORTH_OPEN_AABB = Block.box(0.0D, 0.0D, 15.0D, 16.0D, 16.0D, 16.0D);

    public PaintingBlock() {
        super(Properties.of(Material.WOOL).sound(SoundType.WOOL).instabreak().noOcclusion());
    }

    @SuppressWarnings("deprecation")
    @Override
    @OnlyIn(Dist.CLIENT)
    public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side) {
        return adjacentBlockState.getBlock() instanceof PaintingBlock || super.skipRendering(state, adjacentBlockState, side);
    }

    @Override
    public VoxelShape getShape(BlockState blockState, BlockGetter reader, BlockPos blockPos, CollisionContext context) {

        switch (blockState.getValue(FACING)) {
            case NORTH:
            default:
                return NORTH_OPEN_AABB;
            case SOUTH:
                return SOUTH_OPEN_AABB;
            case WEST:
                return WEST_OPEN_AABB;
            case EAST:
                return EAST_OPEN_AABB;
        }
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder
                .add(FACING)
                .add(PAINTING_ID)
                .add(POSITION);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState()
                .setValue(FACING, context.getHorizontalDirection().getOpposite())
                .setValue(PAINTING_ID, 0)
                .setValue(POSITION, 0);
    }
}
