package com.chirptheboy.largepaintings.block;

import com.chirptheboy.largepaintings.item.PaintBrushItem;
import com.chirptheboy.largepaintings.setup.Config;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.entity.player.Player;
import net.minecraft.core.Direction;
import net.minecraft.world.phys.BlockHitResult;

public class PaintingControllerBlock extends PaintingBlock{

    /** When the painting controller is destroyed, reset all canvases to the blank one */
    @Override
    public void destroy(LevelAccessor pLevel, BlockPos pPos, BlockState pState) {
        updateAllCanvases((Level) pLevel, pPos, pState.getValue(FACING), 0);
        super.destroy(pLevel, pPos, pState);
    }

    @Override
    public InteractionResult use(BlockState blockState, Level world, BlockPos blockPos, Player playerEntity, InteractionHand hand, BlockHitResult blockRayTraceResult) {

        if (playerEntity.getMainHandItem().getItem() instanceof PaintBrushItem) {

            // Get the next value of the paintingId, wrapping around to zero
            int paintingId = blockState.getValue(PAINTING_ID).equals(Config.NUMBER_OF_PAINTINGS.get()) ? 0 : blockState.getValue(PAINTING_ID) + 1;
            Direction facing = world.getBlockState(blockPos).getValue(FACING);
            updateAllCanvases(world, blockPos, facing, paintingId);
            return InteractionResult.sidedSuccess(world.isClientSide);
        }
        else return InteractionResult.PASS;
    }

    public void updateAllCanvases(Level world, BlockPos controllerPos, Direction facing, int paintingId) {

        int position = 0;
        int row;
        int col;
        int x = 0;
        int z = 0;

        for (row = 0; row < PaintingBlock.max_rows; row++){
            for (col = 0; col < PaintingBlock.max_cols; col++){

                switch (facing){
                    case NORTH:
                        x = -1;
                        z = 0;
                        break;
                    case SOUTH:
                        x = 1;
                        z = 0;
                        break;
                    case EAST:
                        x = 0;
                        z = -1;
                        break;
                    case WEST:
                        x = 0;
                        z = 1;
                        break;
                }

                BlockPos affectedBlockPos = controllerPos.offset((col * x), row, (col * z)); // This can use truncated division and remainder (modulo)
                if (world.getBlockState(affectedBlockPos).getBlock() instanceof PaintingBlock) {
                    BlockState affectedBlockState = world.getBlockState(affectedBlockPos).setValue(POSITION, position).setValue(PAINTING_ID, paintingId);
                    world.setBlockAndUpdate(affectedBlockPos, affectedBlockState);
                }
                position++;
            }
        }
    }
}
