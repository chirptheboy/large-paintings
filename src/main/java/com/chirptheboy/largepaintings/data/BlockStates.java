package com.chirptheboy.largepaintings.data;

import com.chirptheboy.largepaintings.block.PaintingBlock;
import com.chirptheboy.largepaintings.setup.Registration;
import net.minecraft.core.Direction;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.VariantBlockStateBuilder;
import net.minecraftforge.common.data.ExistingFileHelper;

import static com.chirptheboy.largepaintings.LargePaintings.modID;

public class BlockStates extends BlockStateProvider {

    public BlockStates(DataGenerator gen, ExistingFileHelper exFileHelper) {
        super(gen, modID, exFileHelper);
    }


    @Override
    protected void registerStatesAndModels() {

        final int paintings = PaintingBlock.paintings; // 8
        final int positions = PaintingBlock.positions; // 64
        final int max_rows = PaintingBlock.max_rows;
        final int max_cols = PaintingBlock.max_cols;
        final int offset = 2;

        // The blockstate builders for the painting and canvas
        VariantBlockStateBuilder canvas_builder = getVariantBuilder(Registration.CANVAS_BLOCK.get());
        VariantBlockStateBuilder painting_builder = getVariantBuilder(Registration.PAINTING_BLOCK.get());

        /* Loop through the # of paintings
         * Painting 0 is the blank canvas, while Painting > 1 are actual paintings */
        for (int painting = 0; painting <= paintings; painting++) {

            int position = 0;

            // Loop through the positions
            for (int row = 0; row < max_rows; row++) {

                // Loop through the columns
                for (int col = 0; col < max_cols; col++) {

                    // b for "blank," meaning no painting is used (UV will be full 16x16)
                    boolean b = painting == 0;

                    // Create the canvas models for each position (row x column)
                    models().getBuilder("canvas_" + painting + "_" + position)
                            .parent(new ModelFile.UncheckedModelFile(modLoc("block/canvas_block")))
                            .texture("painting", b ? mcLoc("block/white_wool") : modLoc("block/painting_" + painting))
                            .element()
                            .from(0, 0, 0)
                            .to(16, 16, 1)
                            .face(Direction.SOUTH).texture("#painting")
                            .uvs(
                                    b ? 0 : offset * col,
                                    b ? 0 : (max_rows - row - 1) * offset,
                                    b ? 16 : offset * (col + 1),
                                    b ? 16 : (max_rows - row) * offset)
                            .end()
                            .face(Direction.NORTH).texture("#canvas_block").cullface(Direction.NORTH).end()
                            .face(Direction.EAST).texture("#canvas_block").cullface(Direction.EAST).end()
                            .face(Direction.WEST).texture("#canvas_block").cullface(Direction.WEST).end()
                            .face(Direction.UP).texture("#canvas_block").cullface(Direction.UP).end()
                            .face(Direction.DOWN).texture("#canvas_block").cullface(Direction.DOWN).end();

                    for (Direction dir : PaintingBlock.FACING.getPossibleValues()) {
                        int angle = (int) dir.toYRot();

                        /*
                           Add the Canvas blockstates for each direction. Use generic canvas model
                           once the loop has exceeded this model's given size
                         */
                        canvas_builder
                                .partialState()
                                .with(PaintingBlock.FACING, dir)
                                .with(PaintingBlock.PAINTING_ID, painting)
                                .with(PaintingBlock.POSITION, position)
                                .modelForState()
                                .modelFile(new ModelFile.UncheckedModelFile(modLoc("block/canvas_" + (b ? "block" : painting + "_" + position))))
                                .rotationY(angle)
                                .addModel();

                        /*
                          Add the Controller blockstates, but only need to run once each painting,
                          not each painting/row/column, so run on row 0 column 0.

                          For no painting (b), use the controller model, otherwise the canvas model we created above
                         */
                        if (position == 0) {
                            painting_builder
                                    .partialState()
                                    .with(PaintingBlock.FACING, dir)
                                    .with(PaintingBlock.PAINTING_ID, painting)
                                    .modelForState()
                                    .modelFile(new ModelFile.UncheckedModelFile(modLoc(b ? "block/painting_block" : "block/canvas_" + painting + "_0")))
                                    .rotationY(angle)
                                    .addModel();
                        }
                    }
                    position++;
                } // Column loop end
            } // Row loop end
        } // Painting loop end
    }
}
