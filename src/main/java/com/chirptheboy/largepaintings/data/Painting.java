package com.chirptheboy.largepaintings.data;

public class Painting {

    private int painting_id;
    private int rows;
    private int cols;

    public Painting(int painting_id, int rows, int cols) {
        this.painting_id = painting_id;
        this.rows = rows;
        this.cols = cols;
    }
}
