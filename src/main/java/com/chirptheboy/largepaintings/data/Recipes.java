package com.chirptheboy.largepaintings.data;

import com.chirptheboy.largepaintings.setup.Registration;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.tags.ItemTags;
import net.minecraftforge.common.Tags;

import java.util.function.Consumer;

public class Recipes extends RecipeProvider {
    public Recipes(DataGenerator generatorIn) {
        super(generatorIn);
    }

    @Override
    protected void buildCraftingRecipes(Consumer<FinishedRecipe> consumer) {

        /* Canvas block recipe */
        ShapedRecipeBuilder.shaped(Registration.CANVAS_BLOCK.get(), 8)
                .pattern("xxx")
                .pattern("x#x")
                .pattern("xxx")
                .define('#', ItemTags.WOODEN_PRESSURE_PLATES)
                .define('x', ItemTags.WOOL)
                .group("largepaintings")
                .unlockedBy("has_planks", has(ItemTags.WOODEN_PRESSURE_PLATES))
                .save(consumer);

        /* Painting block recipe */
        ShapedRecipeBuilder.shaped(Registration.PAINTING_BLOCK.get())
                .pattern("xxx")
                .pattern("x#x")
                .pattern("xxx")
                .define('#', Registration.CANVAS_BLOCK.get())
                .define('x', Tags.Items.RODS_WOODEN)
                .unlockedBy("has_planks", has(ItemTags.WOODEN_PRESSURE_PLATES))
                .group("largepaintings")
                .save(consumer);

        /* Paintbrush recipe */
        ShapedRecipeBuilder.shaped(Registration.PAINT_BRUSH.get())
                .pattern("ttt")
                .pattern(" b ")
                .pattern(" s ")
                .define('s', Tags.Items.RODS_WOODEN)
                .define('b', ItemTags.PLANKS)
                .define('t', Tags.Items.STRING)
                .unlockedBy("has_planks", has(Tags.Items.RODS_WOODEN))
                .group("largepaintings")
                .save(consumer);
    }

}
